package com.hakam.excercise1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.RadioButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var OPERATOR: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupListener()
    }

    private fun validate(): Boolean {
        if (editTextValue1.text.isNullOrEmpty() || editTextValue2.text.isNullOrEmpty()) {
            return false
        } else if(OPERATOR.isNullOrEmpty()){
            return false
        }
        return true
    }

    private fun calculator(value1: Int, value2: Int): String {
        var result: Int = 0
        when(OPERATOR){
            "+" -> result = value1 + value2
            "-" -> result = value1 - value2
            ":" -> result = value1 / value2
            "x" -> result = value1 * value2
        }
        return result.toString()
    }

    private fun setupListener() {
        btnCalculate.setOnClickListener {
            if (validate()) {
                val value1: Int = editTextValue1.text.toString().toInt()
                val value2: Int = editTextValue2.text.toString().toInt()
                textResult.text = calculator(value1, value2)
            } else {
                showMessage("Masukkan data dengan benar!")
            }
        }
        radioGroup.setOnCheckedChangeListener { radioGroup, i ->
            val radioButton = findViewById<RadioButton>(radioGroup.checkedRadioButtonId)
            OPERATOR = radioButton.text.toString()
            textResult.text = getString(R.string.hasil)
        }
    }

    private fun showMessage(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }
}